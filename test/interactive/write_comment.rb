require_relative './interactive_init'

comment_id = Identifier::UUID::Random.get

video_id = '123'
commenter_id = 'abc'
body = 'body'
comment_time = '2020-06-01T04:33:00'

Controls::Write::Comment.(
  comment_id: comment_id,
  commenter_id: commenter_id,
  video_id: video_id,
  body: body,
  comment_time: comment_time
)
