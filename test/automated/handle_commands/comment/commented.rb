require_relative '../../automated_init'

context "Handle Commands" do
  context "Comment" do
    context "Commented" do
      handler = Handlers::Commands.new

      comment = Controls::Commands::Comment.example

      comment_id = comment.comment_id or fail
      video_id = comment.video_id or fail
      commenter_id = comment.commenter_id or fail
      body = comment.body or fail
      comment_time = comment.comment_time or fail
      global_position = comment.metadata.global_position or fail

      handler.(comment)

      write = handler.write

      commented = write.one_message do |event|
        event.instance_of?(Messages::Events::Commented)
      end

      test "Commented Event is Written" do
        refute(commented.nil?)
      end

      test "Written to the videoComment stream" do
        written_to_stream = write.written?(commented) do |stream_name|
          stream_name == "videoComment-#{comment_id}"
        end

        assert(written_to_stream)
      end

      context "Attributes" do
        test "comment_id" do
          assert(commented.comment_id == comment_id)
        end

        test "video_id" do
          assert(commented.video_id == video_id)
        end

        test "commenter_id" do
          assert(commented.commenter_id == commenter_id)
        end

        test "body" do
          assert(commented.body == body)
        end

        test "comment_time" do
          assert(commented.comment_time == comment_time)
        end
      end
    end
  end
end
