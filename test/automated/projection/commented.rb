require_relative '../automated_init'

context "Projection" do
  context "Commented" do
    commented = Controls::Events::Commented.example

    video_comment = Controls::VideoComment::New.example

    Projection.(video_comment, commented)

    test "ID is set" do
      assert(video_comment.id == commented.comment_id)
    end

    test "Has been commented" do
      assert(video_comment.commented?)
    end
  end
end
