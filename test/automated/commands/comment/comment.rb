require_relative '../../automated_init'

context "Commands" do
  context "Deposit" do
    comment_id = Controls::ID.example
    video_id = Controls::Video.id
    commenter_id = Controls::Commenter.id
    comment_time = Controls::Time::Raw.example
    body = Controls::VideoComment.body

    comment = Commands::Comment.new
    comment.clock.now = comment_time

    comment.(comment_id: comment_id, video_id: video_id, commenter_id: commenter_id, comment_time: comment_time, body: body)

    write = comment.write

    comment_message = write.one_message do |written|
      written.instance_of?(Messages::Commands::Comment)
    end

    test "Comment command is written" do
      refute(comment_message.nil?)
    end

    test "Written to the video_comment command stream" do
      written_to_stream = write.written?(comment_message) do |stream_name|
        stream_name == "videoComment:command-#{comment_id}"
      end

      assert(written_to_stream)
    end

    context "Attributes" do
      test "comment_id is assigned" do
        assert(comment_message.comment_id == comment_id)
      end

      test "video_id" do
        assert(comment_message.video_id == video_id)
      end

      test "commenter_id" do
        assert(comment_message.commenter_id == commenter_id)
      end

      test "body" do
        assert(comment_message.body == body)
      end

      test "comment_time" do
        comment_time_iso8601 = Clock.iso8601(comment_time)

        assert(comment_message.comment_time == comment_time_iso8601)
      end
    end

  end
end
