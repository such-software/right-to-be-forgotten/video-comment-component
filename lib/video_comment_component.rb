require 'eventide/postgres'
require 'consumer/postgres'
require 'try'

require 'video_comment_component/load'

require 'video_comment_component/video_comment'
require 'video_comment_component/projection'
require 'video_comment_component/store'

require 'video_comment_component/handlers/commands'

require 'video_comment_component/consumers/commands'

require 'video_comment_component/start'
