module VideoCommentComponent
  module Controls
    module Time
      def self.example(time=nil)
        time ||= Raw.example
        ISO8601.example(time)
      end

      module Raw
        def self.example
          Clock::Controls::Time::Raw.example
        end
      end

      module ISO8601
        def self.example(time=nil)
          Clock::Controls::Time::ISO8601.example(time)
        end

        def self.precision
          3
        end
      end
    end
  end
end
