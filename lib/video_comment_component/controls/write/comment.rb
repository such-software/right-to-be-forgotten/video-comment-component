module VideoCommentComponent
  module Controls
    module Write
      module Comment
        def self.call(comment_id: nil, commenter_id: nil, video_id: nil, body: nil, comment_time: nil)
          comment_id ||= VideoComment.id

          comment = Commands::Comment.example(
            comment_id: comment_id,
            commenter_id: commenter_id,
            video_id: video_id,
            body: body,
            comment_time: comment_time
          )

          stream_name = Messaging::StreamName.command_stream_name(
            comment_id,
            'videoComment'
          )

          Messaging::Postgres::Write.(comment, stream_name)
        end
      end
    end
  end
end
