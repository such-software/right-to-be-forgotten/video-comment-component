module VideoCommentComponent
  module Controls
    module VideoComment
      def self.example
        video_comment = VideoCommentComponent::VideoComment.build

        video_comment.id = id
        video_comment.commenter_id = Commenter.id
        video_comment.video_id = Video.id
        video_comment.comment_time =  Time.example
        video_comment.body = body

        video_comment
      end

      def self.id
        ID.example(increment: id_increment)
      end

      def self.id_increment
        111
      end

      def self.body
        'a comment'
      end

      module New
        def self.example
          VideoCommentComponent::VideoComment.build
        end
      end
    end
  end
end
