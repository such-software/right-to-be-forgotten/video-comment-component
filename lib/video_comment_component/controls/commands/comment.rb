module VideoCommentComponent
  module Controls
    module Commands
      module Comment 
        def self.example(comment_id:  nil, video_id: nil, commenter_id: nil, body: nil, comment_time: nil)
          comment_id ||=  VideoComment.id
          video_id ||= Video.id
          commenter_id ||= Commenter.id
          body ||= VideoComment.body
          comment_time ||= Controls::Time.example

          comment = VideoCommentComponent::Messages::Commands::Comment.build

          comment.comment_id = comment_id
          comment.video_id = video_id
          comment.commenter_id = commenter_id
          comment.body = body
          comment.comment_time = comment_time

          comment.metadata.global_position = Position.example

          comment
        end
      end
    end
  end
end
