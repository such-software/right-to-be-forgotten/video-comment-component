module VideoCommentComponent
  module Controls 
    module Events
      module Commented
        def self.example
          commented = VideoCommentComponent::Messages::Events::Commented.build

          commented.comment_id = VideoComment.id
          commented.video_id = Video.id
          commented.commenter_id = Commenter.id
          commented.body = VideoComment.body
          commented.comment_time = Controls::Time.example

          commented
        end
      end
    end
  end
end
