module VideoCommentComponent
  module Messages
    module Events
      class Commented
        include Messaging::Message

        attribute :comment_id, String
        attribute :video_id, String
        attribute :commenter_id, String
        attribute :comment_time, String
        attribute :body, String
      end
    end
  end
end
