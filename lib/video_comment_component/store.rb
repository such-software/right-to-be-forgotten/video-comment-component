module VideoCommentComponent
  class Store
    include EntityStore

    category :video_comment
    entity VideoComment
    projection Projection
    reader MessageStore::Postgres::Read
  end
end
