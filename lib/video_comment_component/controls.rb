require 'clock/controls'
require 'identifier/uuid/controls'
require 'messaging/controls'

require 'video_comment_component/controls/id'
require 'video_comment_component/controls/time'
require 'video_comment_component/controls/message'
require 'video_comment_component/controls/position'

require 'video_comment_component/controls/commands/comment'

require 'video_comment_component/controls/events/commented'

require 'video_comment_component/controls/write/comment'

require 'video_comment_component/controls/video_comment'
require 'video_comment_component/controls/commenter'
require 'video_comment_component/controls/video'
