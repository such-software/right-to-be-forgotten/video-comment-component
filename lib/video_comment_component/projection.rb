module VideoCommentComponent
  class Projection
    include EntityProjection
    include Messages::Events

    entity_name :video_comment

    apply Commented do |commented|
      video_comment.id = commented.comment_id
      video_comment.commenter_id = commented.commenter_id
      video_comment.video_id = commented.video_id
      video_comment.body = commented.body
      video_comment.comment_time = Time.parse(commented.comment_time)
    end

  end
end
