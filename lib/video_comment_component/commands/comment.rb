module VideoCommentComponent
  module Commands
    class Comment
      include Command

      def self.configure(receiver, attr_name: nil)
        attr_name ||= :comment
        instance = build
        receiver.public_send("#{attr_name}=", instance)
      end

      def self.call(comment_id:, video_id:, commenter_id:, comment_time:, body:, previous_message: nil)
        comment_id ||= Identifier::UUID::Random.get
        instance = self.build
        instance.(comment_id: comment_id, video_id: account_id, commenter_id: commenter_id, comment_time: comment_time, body: body, previous_message: previous_message)
      end

      def call(comment_id:, video_id:, commenter_id:, comment_time:, body:, previous_message: nil)
        comment = self.class.build_message(Messages::Commands::Comment, previous_message)

        comment.comment_id = comment_id
        comment.video_id = video_id
        comment.commenter_id = commenter_id
        comment.comment_time = clock.iso8601
        comment.body = body 

        stream_name = command_stream_name(comment_id)

        write.(comment, stream_name)

        comment 
      end
    end
  end
end
