module VideoCommentComponent
  module Handlers
    class Commands
      include Log::Dependency
      include Messaging::Handle
      include Messaging::StreamName
      include Messages::Commands
      include Messages::Events

      dependency :write, Messaging::Postgres::Write
      dependency :clock, Clock::UTC
      dependency :store, Store

      def configure
        Messaging::Postgres::Write.configure(self)
        Clock::UTC.configure(self)
        Store.configure(self)
      end

      category :video_comment

      handle Comment do |comment|
        comment_id = comment.comment_id

        videoComment, version = store.fetch(comment_id, include: :version)

        if videoComment.commented?
          logger.info(tag: :ignored) { "Command ignored (Command: #{comment.message_type}, VideoComment ID: #{comment_id}" }
          return
        end

        commented = Commented.follow(comment)

        stream_name = stream_name(comment_id)

        write.(commented, stream_name, expected_version: version)
      end
    end
  end
end
