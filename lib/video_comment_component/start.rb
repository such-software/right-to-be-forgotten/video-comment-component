module VideoCommentComponent
  module Start
    def self.call
      Consumers::Commands.start('videoComment:command')
    end
  end
end
