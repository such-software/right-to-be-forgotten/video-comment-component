module VideoCommentComponent
  class VideoComment
    include Schema::DataStructure

    attribute :id, String
    attribute :commenter_id, String
    attribute :video_id, String
    attribute :comment_time, Time
    attribute :body, String

    def commented?
      !comment_time.nil?
    end
  end
end
