# -*- encoding: utf-8 -*-
Gem::Specification.new do |s|
  s.name = 'video_comment_component'
  s.version = '0.0.0'
  s.summary = 'Video Comment Component'
  s.description = 'Part of a proof of concept around event sourcing and right to be forgotten requests'

  s.authors = ['Ethan Garofolo']
  s.email = 'ethan@suchsoftware.com'
  s.homepage = 'https://gitlab.com/such-software/right-to-be-forgotten/video-comment-component'
  s.licenses = ['MIT']

  s.require_paths = ['lib']
  s.files = Dir.glob('{lib}/**/*')
  s.platform = Gem::Platform::RUBY
  s.required_ruby_version = '>= 2.4.0'

  s.add_runtime_dependency 'eventide-postgres'
  s.add_runtime_dependency 'evt-try'

  s.add_development_dependency 'test_bench'
end
